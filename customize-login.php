<?php
/*
Plugin Name: Customize Login
Plugin URI: http://bitbucket.org/jupitercow/customize-login
Description: Replace log in form. Moves everything to a page template.
Version: 0.2
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2013 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('customize_login') ) :

add_action( 'plugins_loaded', array('customize_login', 'plugins_loaded') );

add_action( 'init', array('customize_login', 'init') );

class customize_login
{
	/**
	 * Class prefix
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $prefix = __CLASS__;

	/**
	 * Current version of plugin
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $version = '0.1';

	/**
	 * Settings
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $settings;

	/**
	 * Holds the error/update action messages
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $messages;

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function init()
	{
		// Get the login page or set it up if we can't find one
		self::$settings['login_slug'] = apply_filters( self::$prefix . '/post_name', 'login' );
		self::get_login_page();

		self::$messages = array(
			'failed' => array(
				'key' => 'action',
				'value' => 'failed',
				'message' => __("There was a problem with your username or password.", self::$prefix),
				'args' => 'error=true&page='.self::$settings['login_slug']
			),
			'loggedout' => array(
				'key' => 'action',
				'value' => 'loggedout',
				'message' => __("You are now logged out.", self::$prefix),
				'args' => 'page='.self::$settings['login_slug']
			),
			'recovered' => array(
				'key' => 'action',
				'value' => 'recovered',
				'message' => __("Check your e-mail for the confirmation link.", self::$prefix),
				'args' => 'page='.self::$settings['login_slug']
			),
			'password' => array(
				'key' => 'action',
				'value' => 'password',
				'message' => __("Please enter your username or email. You will receive a link to create a new password via email.", self::$prefix),
				'args' => 'page='.self::$settings['login_slug']
			),
			'passworderror' => array(
				'key' => 'action',
				'value' => 'passworderror',
				'message' => __("<strong>ERROR</strong>: Enter a username or e-mail address.", self::$prefix),
				'args' => 'error=true&page='.self::$settings['login_slug']
			)
		);
		self::$messages = apply_filters( self::$prefix . '/add_messages', self::$messages );

		// Set up custom log in page
		add_action( 'login_head',                 array(__CLASS__, 'redirect_wp_login'), 1 );
		add_filter( 'login_url',                  array(__CLASS__, 'new_login_url'), 10, 2 );
		add_action( 'wp_login_failed',            array(__CLASS__, 'login_failed'), 10, 2 );
		add_filter( 'lostpassword_url',           array(__CLASS__, 'lostpassword_url'), 10, 2 );

		// Allow emails for log in
		add_filter( 'authenticate',               array(__CLASS__, 'allow_email_login'), 20, 3);

		// Add form to Login Page
		add_filter( 'the_content',                array(__CLASS__, 'add_form_to_login_page') );

		// If user is logged in, redirect the login page to homepage or filter specified
		add_filter( 'wp',                         array(__CLASS__, 'redirect_logged_in') );
	}

	/**
	 * On plugins_loaded test if we can use frontend_notifications
	 *
	 * @author  Jake Snyder
	 * @since	0.2.1
	 * @return	void
	 */
	public static function plugins_loaded()
	{
		// Have the login plugin use frontend notifictions plugin
		if ( apply_filters( self::$prefix . '/use_frontend_notifications', true ) )
		{
			if ( class_exists('frontend_notifications') )
			{
				add_filter( 'frontend_notifications/queries', array(__CLASS__, 'add_login_notifications') );
			}
			else
			{
				add_filter( self::$prefix . '/use_frontend_notifications', '__return_false' );
			}
		}
	}

	/**
	 * Redirect logged in users
	 *
	 * @author  Jake Snyder
	 * @since	0.2
	 * @return	void
	 */
	public static function redirect_logged_in( $queries )
	{
		if ( is_page(self::$settings['login_slug']) && is_user_logged_in() )
		{
			$redirect = home_url('/');
			if ( $logged_in_redirect = apply_filters( self::$prefix . '/logged_in_redirect', false ) )
			{
				if ( is_numeric($logged_in_redirect) )
				{
					$redirect = get_permalink($logged_in_redirect);
				}
				else
				{
					$page = get_page_by_path($logged_in_redirect);
					if ( is_object($page) ) $redirect = get_permalink( $page->ID );
				}
			}

			wp_redirect( $redirect ); die;
		}
	}

	/**
	 * Adds a form to the login page, this can be turned off using the filter: 'customize_login/add_form'
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	string $content The post content for login page with the login form a
	 */
	public static function add_login_notifications( $queries )
	{
		$queries = array_merge($queries, self::$messages);
		return $queries;
	}

	/**
	 * Adds a form to the login page, this can be turned off using the filter: 'customize_login/add_form'
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	string $content The post content for login page with the login form a
	 */
	public static function add_form_to_login_page( $content )
	{
		if ( is_page(self::$settings['login_id']) && is_main_query() && in_the_loop() && apply_filters( self::$prefix . '/add_form', true ) )
		{
			$default_args = array(
				'label_username' => __('Username or Email', self::$prefix)
			);
			$args = array_merge( $default_args, apply_filters( self::$prefix . '/form_args', array() ) );

			$messages = '';
			$password_form = false;
			$show_password_form = array('password','passworderror');

			if (! empty($_GET['action']) )
			{
				$action = $_GET['action'];
				if (! apply_filters( self::$prefix . '/use_frontend_notifications', true ) && apply_filters( self::$prefix . '/show_messages', true ) )
					$messages = (! empty(self::$messages[$action]['message']) ) ? self::$messages[$action]['message'] : '';

				if ( in_array(trim($action), $show_password_form) )
					$password_form = true;
			}

			ob_start();
			if (! empty($password_form) ) : ?>

				<article id="content_page" <?php post_class('clearfix'); ?> role="article">
					<header class="article-header">
						<h2><?php _e("Lose something?", self::$prefix); ?></h2>
					</header>
					<section class="entry-content clearfix" itemprop="articleBody">
						<div class="loginform-container">
							<form name="loginform" id="loginform" action="<?php echo site_url('wp-login.php?action=lostpassword', 'login_post') ?>" method="post">
								<p class="login-username">
									<label for="user_login"><?php _e('Username or Email', self::$prefix); ?></label>
									<input type="text" name="user_login" id="user_login" class="input" value="" size="20" />
								</p>
								<p class="login-submit">
									<?php do_action( 'login_form', 'resetpass' ); ?>
									<input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="<?php _e("Get New Password", self::$prefix); ?>">
									<input type="hidden" name="redirect_to" value="<?php echo add_query_arg('action', 'recovered'); ?>" />
								</p>
							</form>
						</div>
					</section>
				</article>

			<?php else :

				if ( apply_filters( self::$prefix . '/show_lost_password', true ) )
				{
					add_filter( 'login_form_bottom', array(__CLASS__, 'password_link') );
				}
				wp_login_form( $args );

			endif;

			$content = $messages . $content . ob_get_clean();
		}
		return $content;
	}

	/**
	 * Add password recovery link
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	string The html for the password recovery link
	 */
	public static function password_link()
	{
		$output = '';

		ob_start(); ?>
		<p class="password-recover">
			<a href="<?php echo wp_lostpassword_url( add_query_arg('action', 'recovered', self::new_login_url()) ); ?>" title="<?php _e("Recover Lost Password", self::$prefix); ?>">
				<?php echo apply_filters( self::$prefix . '/label_lost_password', __("Lost Password?", self::$prefix) ); ?>
			</a>
		</p>
		<?php $output = ob_get_clean();

		return $output;
	}

	/**
	 * Get the login page
	 *
	 * If not login page exists, create one.
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function get_login_page()
	{
		self::$settings['login_page'] = get_page_by_path(self::$settings['login_slug']);

		if ( is_object(self::$settings['login_page']) )
		{
			self::$settings['login_id'] = self::$settings['login_page']->ID;
		}
		else
		{
			self::$settings['login_page'] = get_page_by_path('login');
			if ( is_object(self::$settings['login_page']) )
			{
				self::$settings['login_page']->post_name = self::$settings['login_slug'];
				self::$settings['login_id'] = wp_update_post( self::$settings['login_page'] );
			}
			else
			{
				$page = array(
					'post_title'     => "Log In",
					'post_name'      => self::$settings['login_slug'],
					'post_content'   => "",
					'menu_order'     => 0,
					'post_status'    => 'publish',
					'post_type'      => 'page',
					'comment_status' => 'closed',
					'ping_status'    => 'closed'
				);
				self::$settings['login_id'] = wp_insert_post( $page );
			}
		}
	}

	/**
	 * Redirect wp_login.php to the new login page
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function redirect_wp_login()
	{
		if ( 'wp-login.php' == $GLOBALS['pagenow'] )
		{
			$redirect_url = self::new_login_url();

			if (! empty($_GET['action']) )
			{
				if ( 'rp' == $_GET['action'] )
				{
					return;
				}
				elseif ( 'lostpassword' == $_GET['action'] )
				{
					$redirect_url = add_query_arg( 'action', 'passworderror', self::new_login_url() );
				}
				elseif ( 'register' == $_GET['action'] )
				{
					$page = get_page_by_path('register');
					if ( $page ) $redirect_url = get_permalink($page->ID);
				}
			}
			elseif (! empty($_GET['loggedout'])  )
			{
				$redirect_url = add_query_arg('action', 'loggedout', self::new_login_url());
			}

			wp_redirect( $redirect_url );
			exit;
		}
	}

	/**
	 * Redirect lost password
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	string New lost password url
	 */
	public static function lostpassword_url( $lostpassword_url, $redirect )
	{
		return add_query_arg( 'action', 'password', self::new_login_url( wp_login_url($redirect), $redirect ) );
	}

	/**
	 * New Login URL
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function new_login_url( $login_url='', $redirect='' )
	{
		$login_url = get_permalink(self::$settings['login_id']);
		if (! empty($redirect) ) $login_url = add_query_arg('redirect_to', urlencode($redirect), $login_url);

		return $login_url;
	}

	/**
	 * Redirect login failed
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function login_failed( $username )
	{
		$referrer = wp_get_referer();

		if ( $referrer && ! strstr($referrer, 'wp-login') && ! strstr($referrer, 'wp-admin') )
		{
			wp_redirect( add_query_arg('action', 'failed', self::new_login_url()) );
			die;
		}
	}

	/**
	 * Allow email address log in instead of username
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function allow_email_login( $user, $username, $password ) {
		if ( is_email($username) )
		{
			$user = get_user_by( 'email', $username );
			if ( $user ) $username = $user->user_login;
		}
		return wp_authenticate_username_password( null, $username, $password );
	}
}

endif;